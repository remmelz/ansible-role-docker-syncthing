#!/bin/bash

_name=$1
_size=124
_fstype="ext3"

##########################################

if [[ -z ${_name} ]]; then
  echo "Error: No name given."
  exit 1
fi

if [[ -f ${_name}.fs ]]; then
  echo "Error: Already exists."
  exit 1
fi

if [[ -d ${_name} ]]; then
  echo "Error: Folder name already exists."
  exit 1
fi

##########################################

[[ -n $2 ]] && _size=$2

_folder=`pwd`
_owner=`ls -ld ${_folder} | awk -F' ' '{print $3}'`

##########################################

dd if=/dev/zero of=${_name}.fs bs=1M count=${_size} || exit
mkfs -t ${_fstype} ${_name}.fs

mkdir -v ${_name}

echo "${_folder}/${_name}.fs  ${_folder}/${_name}   ${_fstype}   defaults   0 2   # Syncthing" >> /etc/fstab

mount ./${_name}
chown -v ${_owner}. ./${_name}

cd ./${_name} && df -h .

